################################
reStructuredText Specifications
################################

This project implements and documents reStructuredText
specifications as used by the `Avocet
<https://gitlab.com/avocet-tools/avocet>`__ compiler.

Status
******

In the current release, this content is raw RST documentation
built around the ``spec`` domain. In a future release, the
Avocet compiler will use this content to build a documentation
site as well as configure itself to handle domain specific
operations. This content is being released early to provide the
compiler something to run against in testing and development.
